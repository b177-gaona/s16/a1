/*
1. In the s16 folder, create an a1 folder and an index.html and index.js file inside of it.
2. Link the index.js file to the index.html file.
3. Create a variable number that will store the value of the number provided by the user via the prompt.
4. Create a condition that if the current value is less than or equal to 50, stop the loop.
5. Create another condition that if the current value is divisible by 10, print a message that the number is being skipped and continue to the next iteration of the loop.
6. Create another condition that if the current value is divisible by 5, print the number.
*/

let tNumber = Number(prompt("Give me a number"));
console.log ("The number you provided is " +  tNumber);

for (let i=tNumber; i >= 0; i--) {
	// console.log(tNumber + " " + i);
	if (i <= 50) {
		console.log("The current value is at 50. Terminating the loop.");
		break;
	}
	if(i % 10 === 0) { 
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}	
	if (i % 5 === 0){
		console.log(i);
		continue;
		}		
}

/*
7. Create a variable that will contain the string supercalifragilisticexpialidocious.
8. Create another variable that will store the consonants from the string.
9. Create another for Loop that will iterate through the individual letters of the string based on it’s length.
10. Create an if statement that will check if the letter of the string is equal to a vowel and continue to the next iteration of the loop if it is true.
11. Create an else statement that will add the letter to the second variable.
12. Create a gitlab project repository named as a1 in s16 gitlab subgroup.
13. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
14. Add the link in Boodle.
*/

let tString = "supercalifragilisticexpialidocious"
console.log(tString);
// let tStringVowels = '';
let tStringConsonants = '';

for (let i = 0; i < tString.length; i++) {
	if(
		tString[i].toLowerCase() == 'a' ||
		tString[i].toLowerCase() == 'e' ||
		tString[i].toLowerCase() == 'i' ||
		tString[i].toLowerCase() == 'o' ||
		tString[i].toLowerCase() == 'u' 
	) {
		continue;
		// used for checking
		// tStringVowels= tStringVowels + tString[i];	
		// console.log(tStringVowels.length + "_" + tString[i] + "_" + tStringVowels);
	}
	else {
		// used for checking
		tStringConsonants= tStringConsonants + tString[i];
		// console.log(tStringConsonants.length + tString[i] + tStringConsonants);
	}
}

// console.log(tStringVowels);
console.log(tStringConsonants);